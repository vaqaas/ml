using System;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;
using Microsoft.ML.Transforms.Categorical;
using Microsoft.ML.Transforms.Normalizers;
using Microsoft.ML.Transforms.Text;

namespace TaxiFare
{
    public class Variables
    {
        public static readonly string _trainDataPath = Path.Combine(Environment.CurrentDirectory, "Data", "Train.csv");
        public static readonly string _testDataPath = Path.Combine(Environment.CurrentDirectory, "Data", "Test.csv");
        public static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "Model.zip");
        public static TextLoader _textLoader;        
    }
}