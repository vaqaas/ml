﻿using System;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;
using Microsoft.ML.Transforms.Categorical;
using Microsoft.ML.Transforms.Normalizers;
using Microsoft.ML.Transforms.Text;

namespace TaxiFare
{
    class Program
    {
        static void Main(string[] args)
        {
            Variables._textLoader = Model.mlContext.Data.CreateTextReader(new TextLoader.Arguments()
                    {
                        Separator = ",",
                        HasHeader = true,
                        Column = new[]
                                    {
                                        new TextLoader.Column("VendorId", DataKind.Text, 0),
                                        new TextLoader.Column("RateCode", DataKind.Text, 1),
                                        new TextLoader.Column("PassengerCount", DataKind.R4, 2),
                                        new TextLoader.Column("TripTime", DataKind.R4, 3),
                                        new TextLoader.Column("TripDistance", DataKind.R4, 4),
                                        new TextLoader.Column("PaymentType", DataKind.Text, 5),
                                        new TextLoader.Column("FareAmount", DataKind.R4, 6)
                                    }
                    });

            Model.Train();

            Model.Save();

            Model.Evaluate();

            Model.Predict();
        }
    }
}
