using Microsoft.ML.Data;

namespace TaxiFare
{
    public class TaxiTripFarePrediction
    {
        [Column("Score")]
        public float FareAmount;
    }
}