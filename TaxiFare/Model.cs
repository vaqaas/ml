using System;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;
using Microsoft.ML.Transforms.Categorical;
using Microsoft.ML.Transforms.Normalizers;
using Microsoft.ML.Transforms.Text;

namespace TaxiFare
{
    public class Model
    {
        public static MLContext mlContext = new MLContext(seed: 0);
        private static ITransformer model;
        public static void Train() 
        {
            IDataView dataView = Variables._textLoader.Read(Variables._trainDataPath);

            var pipeline = mlContext.Transforms.CopyColumns("FareAmount", "Label")
            .Append(mlContext.Transforms.Categorical.OneHotEncoding("VendorId"))
            .Append(mlContext.Transforms.Categorical.OneHotEncoding("RateCode"))
            .Append(mlContext.Transforms.Categorical.OneHotEncoding("PaymentType"))
            .Append(mlContext.Transforms.Concatenate("Features", "VendorId", "RateCode", "PassengerCount", "TripTime", "TripDistance", "PaymentType"))
            .Append(mlContext.Regression.Trainers.FastTree());

            model = pipeline.Fit(dataView);
        }
        public static void Save()
        {
            try 
            {
                using(var fileStream = new FileStream(Variables._modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
                    mlContext.Model.Save(model, fileStream);

                Console.WriteLine("Model saved to {0}", Variables._modelPath);
            } 
            catch(InvalidOperationException e)
            {
                Console.WriteLine("Model is not (yet) defined: {0}", e.ToString());
            }
        }
        public static void Evaluate()
        {
            IDataView dataView = Variables._textLoader.Read(Variables._testDataPath);
            var predictions = model.Transform(dataView);
            var metrics = mlContext.Regression.Evaluate(predictions, "Label", "Score");

            Console.WriteLine();
            Console.WriteLine($"*************************************************");
            Console.WriteLine($"*       Model quality metrics evaluation         ");
            Console.WriteLine($"*------------------------------------------------");
            Console.WriteLine($"*       R2 Score:      {metrics.RSquared:0.##}");
            Console.WriteLine($"*       RMS loss:      {metrics.Rms:#.##}");
        }
        public static void Load()
        {
            using (var stream = new FileStream(Variables._modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                model = mlContext.Model.Load(stream);
            }
        }
        public static void Predict()
        {
            var taxiTripSample = new TaxiTrip()
            {
                VendorId = "VTS",
                RateCode = "1",
                PassengerCount = 1,
                TripTime = 1140,
                TripDistance = 3.75f,
                PaymentType = "CRD",
                FareAmount = 0
            };
            
            var predictionFunction = model.CreatePredictionEngine<TaxiTrip, TaxiTripFarePrediction>(mlContext);
            var prediction = predictionFunction.Predict(taxiTripSample);

            Console.WriteLine($"**********************************************************************");
            Console.WriteLine($"Predicted fare: {prediction.FareAmount:0.####}, actual fare: 15.5");
            Console.WriteLine($"**********************************************************************");
        }
    }
}